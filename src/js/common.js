'use strict'

//-------- Burger --------
let burger = document.querySelector('.nav__mobile');
let burgerImg = document.querySelector('.nav__mobile>img');
let burgerMenu = document.querySelector('.nav__list');
let burgerLogo = document.querySelector('.header__nav > .logo');

burger.addEventListener('click', (e) => {
    e.preventDefault();
    if (burger.classList.contains('nav__mobile_open')) {
        burger.classList.remove('nav__mobile_open');
        burgerImg.src = "img/svg/burger-close.svg";
        burgerImg.setAttribute('alt', 'close menu')
        burgerMenu.classList.remove('nav_hidden');
        document.body.style.overflow = 'hidden';

    } else {
        burger.classList.add('nav__mobile_open');
        burgerImg.src = "img/svg/burger-open.svg";
        burgerImg.setAttribute('alt', 'open menu')
        burgerMenu.classList.add('nav_hidden');
        document.body.style.overflow = 'auto';

    }
});